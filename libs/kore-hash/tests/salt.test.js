import generateSalt from '../src/salt'

describe('Salt', () => {
  test('Generate defined length', () => {
    expect(generateSalt(10)).toHaveLength(10)
    expect(generateSalt(100)).toHaveLength(100)
  })
})
