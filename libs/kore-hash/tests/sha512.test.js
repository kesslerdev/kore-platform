import sha512 from '../src/sha512'

const salt = '123'
const hashed =
  '6f916079cc02f03e65da81cae69b0fec12695182b5f01e918a19941135bf37cf385a9d6761dbb19115c289d568a3e14aff18520f0ac48bf0dca763d7c9abad5c'

describe('SHA 512', () => {
  test('Generate hash', () => {
    expect(sha512('hello', salt).salt).toBe(salt)
    expect(sha512('hello', salt).hash).toBe(hashed)
  })
})
