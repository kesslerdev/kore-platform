import createHash from '../src'

const salt = '123'
const hasher = createHash('secret')
const hashed = {
  hash:
    '1eb0a192d7d8b30447d64291e78dd1a5980edf9dc8ef739543d2549250721e3f000abe7d0c59400d5c4e3017fcb8c1e0381a4b9be81bf2385fe2ed8cb8e8eae0',
  salt: '123',
}

describe('Hasher', () => {
  test('Generate hash', () => {
    expect(hasher.hash('hello', 58, salt)).toEqual(hashed)
  })

  test('Random salt', () => {
    expect(hasher.hash('hello', 58)).not.toEqual(hasher.hash('hello', 58))
  })

  test('verify', () => {
    expect(hasher.verify('hellO', hashed, 58)).toBe(false)
    expect(hasher.verify('hello', hashed, 58)).toBe(true)
    const ukHashed = hasher.hash('bonjour')

    expect(hasher.verify('bonjour', ukHashed)).toBe(true)
  })
})
