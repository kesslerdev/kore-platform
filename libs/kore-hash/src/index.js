import generateSalt from './salt'
import sha512 from './sha512'

const createHash = secret => ({
  hash: (value, identifier = null, salt = null) => {
    const valueSalt = salt || generateSalt(16)

    return sha512(identifier + value + secret, valueSalt)
  },
  // eslint-disable-next-line max-len
  verify: (value, hash, identifier = null) => sha512(identifier + value + secret, hash.salt).hash === hash.hash,
})

export default createHash
