import { createHmac } from 'crypto'

const sha512 = (value, salt) => {
  const hash = createHmac('sha512', salt)
  hash.update(value)

  return {
    hash: hash.digest('hex'),
    salt,
  }
}

export default sha512
