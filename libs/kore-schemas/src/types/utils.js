import { validTypes, nestedTypes } from './types'

const isValidType = type => validTypes.some(validType => validType === type)

const isNestedType = type => nestedTypes.some(validType => validType === type)

const validateType = type => {
  if (!isValidType(type)) {
    throw new Error(`Invalid type '${type}'`)
  }

  return type
}

const findNested = (normSchema, withArrays = true) => {
  if (!normSchema.type || !isNestedType(normSchema.type)) {
    throw new Error('findNested accept only flat field of nested type')
  }

  const nested = Object.entries(normSchema.fields).filter(kvp => {
    if (kvp[1].type === Array) {
      return withArrays ? isNestedType(kvp[1].items.type) : false
    }
    return isNestedType(kvp[1].type)
  })

  return nested.map(kvp => kvp[0])
}

export {
  isValidType, isNestedType, validateType, findNested,
}
