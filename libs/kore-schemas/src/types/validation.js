import Joi from 'joi'
import { isNestedType } from './utils'
import { Types } from './types'

const buildType = type => {
  let validate
  // eslint-disable-next-line no-use-before-define
  if (isNestedType(type.type)) validate = buildObject(type)
  else if (type.type === String) validate = Joi.string()
  else if (type.type === Number) validate = Joi.number()
  else if (type.type === Boolean) validate = Joi.boolean()
  else if (type.type === Buffer) validate = Joi.object().type(Buffer)
  else if (type.type === Date) validate = Joi.date()
  else if (type.type === Date) validate = Joi.date()
  else if (type.type === Array) {
    validate = Joi.array().items(buildType(type.items).optional())
  } else if (type.type === Types.ObjectId) {
    validate = Joi.string().guid({
      version: ['uuidv4', 'uuidv5'],
    })
  } else {
    validate = Joi.any()
  }

  if (
    type.default === undefined
    && type.type !== Array
    && type.type !== Types.ObjectId
  ) {
    validate = validate.required()
  }

  if (type.validation && typeof type.validation === 'function') validate = type.validation(validate)

  return validate
}

const buildObject = obj => Joi.object().keys(
  Object.entries(obj.fields).reduce(
    (acc, kvp) => Object.assign({}, acc, {
      [kvp[0]]: buildType(kvp[1]),
    }),
    {},
  ),
)

const buildValidator = schema => {
  const validationSchema = buildObject(schema)
  return object => {
    const check = Joi.validate(object, validationSchema)
    if (check.error) {
      throw check.error
    }
    return true
  }
}

export default buildValidator
