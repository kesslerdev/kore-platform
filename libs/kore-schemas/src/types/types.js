const Types = {
  ObjectId: function ObjectId() {},
  Mixed: function Mixed() {},
  Schema: function Mixed() {},
}

const nestedTypes = [Object, Types.Schema]

const validTypes = [
  String,
  Number,
  Date,
  Boolean,
  Buffer,
  Array,
  Types.ObjectId,
  Types.Mixed,
  ...nestedTypes,
]

export { Types, nestedTypes, validTypes }
