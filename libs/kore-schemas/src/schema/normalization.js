import { omit } from 'lodash'
import { validateType, isValidType, isNestedType } from '../types'

const normalizeField = (field, forceType = null) => {
  let normField = {}

  // discovery
  if (Array.isArray(field)) {
    normField = Object.assign(
      {},
      {
        type: Array,
        items: field[0],
      },
    )
  } else if (typeof field === 'object' && !field.type) {
    // { a: String, b: {type Number}}
    normField = Object.assign(
      {},
      {
        ...field.__type, // merge all of __type object
        type: Object,
        fields: field,
      },
    )
  } else if (typeof field === 'object' && field.type) {
    // {type: String}
    // flat field
    normField = Object.assign({}, field, {
      type: validateType(field.type),
    })
  } else {
    // String
    normField = Object.assign(
      {},
      {
        type: validateType(field),
      },
    )
  }

  if (forceType) {
    normField = Object.assign({}, normField, {
      type: validateType(forceType),
    })
  }
  // flattize object fields
  if (isNestedType(normField.type)) {
    // eslint-disable-next-line no-use-before-define
    normField = Object.assign({}, normField, normalizeObject(normField))
  } else if (normField.type === Array) {
    normField = Object.assign({}, normField, {
      items: normalizeField(normField.items),
    })
  }
  // check if is valid type or throw exception
  return normField
}

const normalizeObject = object => {
  const normObject = Object.assign({}, object, {
    fields: omit(object.fields, ['__type']),
  })

  return Object.entries(normObject.fields).reduce((accumulator, kvp) => {
    // support fields: { test: (this)=> {}}
    if (!isValidType(kvp[1]) && typeof kvp[1] === 'function') {
      return Object.assign({}, accumulator, {
        methods: Object.assign({}, accumulator.methods, {
          [kvp[0]]: kvp[1],
        }),
        fields: omit(accumulator.fields, [kvp[0]]),
      })
    }

    return Object.assign({}, accumulator, {
      fields: Object.assign({}, accumulator.fields, {
        [kvp[0]]: normalizeField(kvp[1]),
      }),
    })
  }, normObject)
}

export { normalizeField, normalizeObject }
