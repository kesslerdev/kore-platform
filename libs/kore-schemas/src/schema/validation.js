import Joi from 'joi'
import { Types, validTypes, nestedTypes } from '../types'

const baseType = Joi.object().keys({
  name: Joi.string(),
  description: Joi.string(),
  type: Joi.any()
    .valid(validTypes)
    .required(),
  // default provider
  default: Joi.any(),
  // private provider
  private: Joi.boolean(),
  // TODO: enum for string type only
  enum: Joi.any(),
})

const arrayType = baseType.keys({
  type: Joi.any()
    .valid(Array)
    .required(),
  // eslint-disable-next-line no-use-before-define
  items: [baseType, Joi.lazy(() => arrayType), Joi.lazy(() => nestedType)],
})

const nestedType = baseType.keys({
  type: Joi.any()
    .valid(nestedTypes)
    .required(),
  methods: Joi.object().pattern(Joi.string(), Joi.func()),
  fields: Joi.object().pattern(Joi.string(), [
    baseType,
    arrayType,
    Joi.lazy(() => nestedType),
  ]),
})

const schemaValidation = nestedType.keys({
  type: Joi.any()
    .valid(Types.Schema)
    .required(),
})

export default schemaValidation
