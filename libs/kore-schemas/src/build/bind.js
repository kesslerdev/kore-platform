/* eslint no-param-reassign: 0 */
import { findNested } from '../types'
import { normalizeField } from '../schema'
import { mapObjects } from './map'

const createNestedObjects = (
  instance,
  schema,
  recursivelly = true,
  create = () => true,
) => {
  // normalizeField schema
  const normSchema = normalizeField(schema)

  if (
    typeof instance !== 'object'
    || !normSchema.type
    || normSchema.type !== Object
  ) {
    throw new Error(
      'bindMethods accept only an object instance with flat Object type',
    )
  }

  const nestedFields = findNested(normSchema, true)

  nestedFields.forEach(nested => {
    // if needed we can create the nested object
    if (
      !instance[nested]
      && normSchema.fields[nested].type === Object
      && create(instance, normSchema)
    ) {
      // create it
      instance[nested] = {}
    }

    if (instance[nested] && recursivelly) {
      if (normSchema.fields[nested].type === Object) {
        // if object bind instance
        instance[nested] = createNestedObjects(
          instance[nested],
          normSchema.fields[nested],
          recursivelly,
          create,
        )
      } else if (
        normSchema.fields[nested].type === Array
        && instance[nested].length
      ) {
        // if array bind all childs instances
        instance[nested] = instance[nested].map(el => createNestedObjects(
          el,
          normSchema.fields[nested].items,
          recursivelly,
          create,
        ))
      }
    }
  })

  return instance
}
// eslint-disable-next-line max-len
const bindMethods = (inst, normSchema) => mapObjects(inst, normSchema, (instance, schema) => {
  if (schema.methods) {
    Object.entries(schema.methods).forEach(kvp => {
      instance[kvp[0]] = (...args) => kvp[1](instance, ...args)
    })
  }
})

export { bindMethods, createNestedObjects }
