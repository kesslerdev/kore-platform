import { isNestedType, findNested } from '../types'
import { normalizeField } from '../schema'

// mapObjects:
// to call a cb to all object present in object tree defined by the schema

const mapObjects = (instance, schema, job, first = true) => {
  // normalizeField schema
  const normSchema = normalizeField(schema)

  if (
    typeof instance !== 'object'
    || !normSchema.type
    || !isNestedType(normSchema.type)
  ) {
    throw new Error(
      'bindMethods accept only an object instance with normalized Nested type',
    )
  }

  const jobInstance = job(instance, normSchema, first) || instance

  const nestedFields = findNested(normSchema, true)

  nestedFields.forEach(nested => {
    // if nested exists
    if (jobInstance[nested]) {
      if (isNestedType(normSchema.fields[nested].type)) {
        // if object bind jobInstance
        jobInstance[nested] = mapObjects(
          jobInstance[nested],
          normSchema.fields[nested],
          job,
          false,
        )
      } else if (
        normSchema.fields[nested].type === Array
        && jobInstance[nested].length
      ) {
        // if array bind all childs jobInstances
        // eslint-disable-next-line max-len
        jobInstance[nested] = jobInstance[nested].map(el => mapObjects(el, normSchema.fields[nested].items, job, true))
      }
    }
  })

  return jobInstance
}

// callback, (value, field(schema), parent
// eslint-disable-next-line max-len
const mapFields = (object, normSchema, callback) => mapObjects(object, normSchema, (instance, schema, first) => {
  // first callback on object we cannont act as a field on an instance,
  // we cannot provide a parent object,
  // but if have sub objects we call once with subobject & parent instance
  // object in array are considered first

  const inst = first
    ? callback(instance, schema, false) || instance
    : instance

  Object.entries(schema.fields).forEach(kvp => {
    const field = callback(inst[kvp[0]], kvp[1], inst)
    if (field !== undefined) {
      inst[kvp[0]] = field
    }
  })

  return inst
})

export { mapFields, mapObjects }
