import Joi from 'joi'
import { inspect } from 'util'
import type { Schema, SchemaObject } from 'kore-adaptor'
import { normalizeField, schemaValidation } from '../schema'
import { bindMethods } from './bind'
import { Types } from '../types'

import { callProviders, providers as defaultProviders } from '../providers'

import buildValidator from '../types/validation'

const buildSchema = (schema: Schema, { providers = [] } = {}) => {
  const allProviders = [...defaultProviders, ...providers]
  const normSchema = normalizeField(schema, Types.Schema)
  // validate inflated schema against schema validation
  const check = Joi.validate(normSchema, schemaValidation)
  if (check.error) {
    console.error(inspect(normSchema, false, 5, true))
    throw check.error
  }
  const validator = buildValidator(normSchema)

  const creator = function creator(object: Object) {
    validator(object)
    const instance: SchemaObject = {
      ...object,
      __schema: normSchema,
    }

    // call providers
    callProviders(instance, normSchema, allProviders)

    // bind methods recursivelly, create nested object if needed
    return bindMethods(instance, normSchema)
  }

  creator.validate = obj => validator(obj)

  return creator
}

export default buildSchema
