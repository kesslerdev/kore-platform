/* eslint no-param-reassign: 0 */
import { omit } from 'lodash'
import { isNestedType } from '../types'

const privateMiddleware = (value, field) => {
  if (isNestedType(field.type) && value) {
    value.privateFields = () => {
      const privateFields = []

      Object.entries(field.fields).forEach(kvp => {
        if (kvp[1].private) {
          privateFields.push(kvp[0])
        }
      })

      return ['__schema', ...privateFields]
    }
    value.toObject = (priv = false) => {
      let object = Object.assign({}, omit(value, value.privateFields()))
      Object.entries(field.fields).forEach(kvp => {
        if (kvp[1].private && priv) {
          return
        }

        if (kvp[1].type === Object && object[kvp[0]]) {
          const entry = {}
          entry[kvp[0]] = object[kvp[0]].toObject(priv)
          object = Object.assign({}, object, entry)
        } else if (
          kvp[1].type === Array
          && isNestedType(kvp[1].items.type)
          && object[kvp[0]]
        ) {
          // force for all items in
          const entry = {}
          entry[kvp[0]] = object[kvp[0]].map(el => el.toObject(priv))
          object = Object.assign({}, object, entry)
        }
      })
      return object
    }
    value.toPublicObject = () => value.toObject(true)
  }
}

export default privateMiddleware
