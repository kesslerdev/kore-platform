class Connect {
  constructor() {
    this.stack = []
  }

  add(provider): this {
    this.stack.push(provider)
    return this
  }

  handle(value, field, instance) {
    let index = 0
    const { stack } = this
    let end = value

    const next = () => {
      const layer = stack[index]

      const layerValue = layer(end, field, instance)

      if (layerValue) {
        end = layerValue
      }

      index += 1
      if (!stack[index]) {
        return end
      }
      return next()
    }
    return stack[index] ? next() : value
  }
}

export default Connect
