import uuidv4 from 'uuid/v4'
import { Types } from '../types'

const defaultMiddleware = (value, field) => {
  if (!value && field.default !== undefined) {
    return field.default
  }
  if (!value && field.type === Types.ObjectId) {
    return uuidv4()
  }
  return undefined
}

export default defaultMiddleware
