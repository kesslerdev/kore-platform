import defaultProvider from './default'
import privateProvider from './private'

const providers = [defaultProvider, privateProvider]

export default providers
