import { mapFields } from '../build/map'
import Connect from './middleware'

const callProviders = (instance, schema, providers = []) => {
  const middlewares = new Connect()

  providers.forEach(prov => {
    middlewares.add(prov)
  })
  // eslint-disable-next-line max-len
  return mapFields(instance, schema, (value, field, inst) => middlewares.handle(value, field, inst))
}

export default callProviders
