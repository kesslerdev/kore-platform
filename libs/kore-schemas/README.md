
# SWP Schemas

One schema to rule them all

## Translators WIP

- Mongoose
- Avro
- JSON Schema
- Postgres Migrations

## Providers

provide additionnal features to schemas, validations, data transformations

## Definition

**this section is a fork of mongoose schema documentation, thanks for this clear documentation**

Each schema define an object

```js
const blogSchema = {
    title:  String,
    author: String,
    body:   String,
    comments: [{ body: String, date: Date }], // arrays of nested objects
    comments2: { // same as comment
      type: Array,
      items { body: String, date: Date }
    },
    comments3: { // same as comment
      type: Array,
      items {
        type: Object,
        object: { body: String, date: Date }
      }
    },
    date: { type: Date, default: Date.now }, // types can have default values
    hidden: Boolean,
    meta: { // nested object
      votes: Number,
      favs:  Number
    },
    meta2: { // same as meta
      type: Object,
      object: {
        votes: Number,
        favs:  Number
      }
    }
  }
```
Each key in our code `blogSchema` (same as Object) defines a property in our documents which will be cast to its associated SchemaType.
For example, we've defined a property `title` which will be cast to the String SchemaType and property `date` which will be cast to a `Date` SchemaType.
Keys may also be assigned nested objects containing further key/type definitions like the `meta` property above.

The permitted SchemaTypes are:

- String
- Number
- Date
- Boolean
- Array
- Buffer
- Object

Non Standard type using `import {Types} from 'kore-schemas'`
- Types.ObjectId
- Types.Mixed

```js
const schema1 = {
  test: String // `test` is a path of type String
}

const schema2 = {
  test: { type: String } // `test` is a path of type String
}
```

### Using Providers

In addition to the type property, using providers you can specify additional properties for a path. For example, if you want to lowercase a string before saving using `stringUtils` provider:

```js
import stringUtils from 'kore-schemas/string-utils-provider' // WIP

const schema = {
  __providers:[stringUtils]
  test: {
    type: String,
    lowercase: true // Always convert `test` to lowercase
  }
}
```
The `lowercase` property only works for strings if you had `stringUtils` provider.

Providers can have certain options which apply for all schema types, and some that apply for specific schema types.

refers to providers usage documentations


certain options are available with default providers

- `required`: boolean or function, if true adds a required validator for this property

- `default`: Any or function, sets a default value for the path. If the value is a function, the return value of the function is used as the default.

- `validate`: function, adds a validator function for this property
- `get`: function, defines a custom getter for this property using Object.defineProperty().

- `set`: function, defines a custom setter for this property using Object.defineProperty().

- `alias`: Defines a virtual with the given name that gets/sets this path.

- `description`: for documentation
- `namespace`: part of the fullname of object (can be used in type system)
- `name`: part of the fullname of object Define a name for this object `namespace(org.foo).name(X)` => `org.foo.X`


for compatibility with other translators a schema can have a `__type` property

```js
const schema = {
  integerOnly: {
    type: Number,
    get: v => Math.round(v),
    set: v => Math.round(v),
    default: 0,
    alias: 'i'
  },
  // with this property we can generate documentation with docTranslator
  __type: {
    namespace: 'org.foo',
    name: 'IntegerWrapper',
    description: 'wrap an integer'
  }
}

const Number = buildSchema(schema)

const num = new Number()

num.i // 0
num.integerOnly // 0

num.integerOnly = 2.001
num.integerOnly // 2
num.i // 2
```
