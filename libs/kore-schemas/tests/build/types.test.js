import {
  isNestedType,
  isValidType,
  validateType,
  validTypes,
  Types,
} from '../../src/types'


const invalids = [
  Function,
  'name',
  1,
  false,
  () => {},
  [],
]

describe('Types functions', () => {
  test('isNestedType(Object) = true, false another', () => {
    validTypes.forEach(valid => {
      expect(isNestedType(valid))
        .toBe(valid === Object || valid === Types.Schema)
    })

    invalids.forEach(invalid => {
      expect(isNestedType(invalid)).toBe(false)
    })
  })


  test('isValidType(Any of valid types) = true, false another', () => {
    validTypes.forEach(valid => {
      expect(isValidType(valid)).toBe(true)
    })

    invalids.forEach(invalid => {
      expect(isValidType(invalid)).toBe(false)
    })
  })

  test('validateType(Any of valid types) = Type, Exception another', () => {
    validTypes.forEach(valid => {
      expect(validateType(valid)).toBe(valid)
    })

    invalids.forEach(invalid => {
      expect(() => {
        validateType(invalid)
      }).toThrow()
    })
  })
})
