import { normalizeField } from '../../src/schema'
import { Types, findNested } from '../../src/types'

describe('find', () => {
  test('only objects', () => {
    const flatNestedArray = normalizeField([{ id: Types.ObjectId }])

    expect(() => {
      findNested(flatNestedArray)
    }).toThrow()
  })

  test('nested objects', () => {
    const flatNestedArray = normalizeField({
      subobjects: [{ id: Types.ObjectId }],
    })
    const flatNested = normalizeField({ subtype: { id: Types.ObjectId } })

    expect(findNested(flatNestedArray)).toContain('subobjects')
    expect(findNested(flatNestedArray, false)).toHaveLength(0)
    expect(findNested(flatNested)).toContain('subtype')
  })
})
