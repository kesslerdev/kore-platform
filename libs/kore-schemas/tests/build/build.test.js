import { buildSchema } from '../../src/build'
import { Types } from '../../src/types'

describe('build', () => {
  const schema = {
    id: Types.ObjectId,
    test: () => true,
  }

  const BuiltSchema = buildSchema(schema)

  const object = new BuiltSchema()

  test('object includes schemas', () => {
    expect(object.__schema.type).toBe(Types.Schema)
  })

  test('object as boud methods', () => {
    expect(object.test()).toBe(true)
  })
})

describe('build complex', () => {
  test('recursive schema', () => {
    expect(buildSchema({
      name: { type: String, default: 'guest' },
      password: { type: String, default: _ => 55555, private: true },
      identity: {
        first: { type: String },
        last: { type: String },
      },
    })).not.toThrow()
  })
})

describe('test complex', () => {
  test('recursive schema', () => {
    const Schm = buildSchema({
      name: { type: String, default: 'guest' },
      password: { type: String, default: _ => 55555, private: true },
      identities: [
        {
          first: { type: String },
          last: { type: String },
          age: Number,
        },
      ],
    })
    expect(() => {
      const obj = new Schm({
        identities: [
          {
            first: 'John',
            last: 'last',
            age: 58,
          },
        ],
      })
    }).not.toThrow()

    expect(() => {
      Schm.validate({})
    }).not.toThrow()

    expect(() => {
      Schm.validate({
        identities: [
          {
            name: 'John',
          },
        ],
      })
    }).toThrow()
  })
})
