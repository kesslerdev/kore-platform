import { Types } from '../../src/types'

import { normalizeField } from '../../src/schema'

describe('normalizeField', () => {
  test('flattize invalid', () => {
    expect(() => {
      normalizeField(Function)
    }).toThrow()

    expect(() => {
      normalizeField({ type: Function, lowercase: true })
    }).toThrow()
  })

  test('flattize scalar', () => {
    const flatString = normalizeField(String)

    expect(flatString.type).toBe(String)
  })

  test('flattize array, array of objects', () => {
    const flatArray = normalizeField([String])

    expect(flatArray.type).toBe(Array)
    expect(flatArray.items.type).toBe(String)

    const flatNestedArray = normalizeField([{ id: Types.ObjectId }])

    expect(flatNestedArray.type).toBe(Array)
    expect(flatNestedArray.items.type).toBe(Object)
    expect(flatNestedArray.items.fields.id.type).toBe(Types.ObjectId)
  })

  test('flattize object', () => {
    const flatObject = normalizeField({
      id: Types.ObjectId,
      __type: { name: 'TestObject' },
    })

    expect(flatObject.type).toBe(Object)
    expect(flatObject.fields.id.type).toBe(Types.ObjectId)
    expect(flatObject.name).toBe('TestObject')
    expect(flatObject.fields.__type).toBeUndefined()
  })

  test('flattize object methods', () => {
    const test = () => true
    const flatObject = normalizeField({ test })

    expect(flatObject.type).toBe(Object)
    expect(flatObject.methods.test).toBe(test)
    expect(flatObject.fields.test).toBe(undefined)
  })

  test('flattize with opts', () => {
    const flatString = normalizeField({ type: String, lowercase: true })

    expect(flatString.type).toBe(String)
    expect(flatString.lowercase).toBe(true)
  })
})
