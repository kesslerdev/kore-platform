import { normalizeField } from '../../src/schema'
import { bindMethods, createNestedObjects, mapFields } from '../../src/build'
import { Types } from '../../src/types'

describe('bind methods', () => {
  test('only objects with methods', () => {
    const normString = normalizeField(String)

    expect(() => {
      bindMethods({}, normString)
    }).toThrow()

    expect(() => {
      bindMethods('')
    }).toThrow()

    const object = bindMethods({}, {})

    expect(Object.entries(object)).toHaveLength(0)
  })

  test('bind local methods, with first parameter instance', () => {
    const normObject = normalizeField({
      value: Number,
      test: e => e.value || true,
    })

    const object = bindMethods({}, normObject)
    const object2 = bindMethods({ value: 3 }, normObject)

    expect(object.test()).toBe(true)
    expect(object2.test()).toBe(3)
  })

  test('bind nested methods', () => {
    const normNestedObject = normalizeField({
      meta: {
        value: Number,
        test: e => e.value,
      },
    })

    const object = bindMethods({}, normNestedObject)
    const object2 = bindMethods({ meta: { value: 5 } }, normNestedObject)

    expect(object.meta).toBe(undefined)
    expect(object2.meta.test()).toBe(5)
  })

  test('bind nested array methods', () => {
    const normNestedObject = normalizeField({
      metas: [
        {
          value: Number,
          test: e => e.value || true,
        },
      ],
    })

    const object = bindMethods({}, normNestedObject)
    const object2 = bindMethods({ metas: [] }, normNestedObject)
    const object3 = bindMethods({ metas: [{ value: 5 }, {}] }, normNestedObject)

    expect(object.metas).toBe(undefined)
    expect(object2.metas).toHaveLength(0)
    expect(object3.metas).toHaveLength(2)
    expect(object3.metas[0].value).toBe(5)
    expect(object3.metas[0].test()).toBe(5)
    expect(object3.metas[1].test()).toBe(true)
  })
})

describe('mapFields (for providers)', () => {
  test('mapFields to init default', () => {
    const schema = normalizeField({ id: Types.ObjectId })
    const object = mapFields({}, schema, (value, field) => {
      if (field.type == Types.ObjectId) {
        return 55 // arbitrary id
      }
    })
    expect(object.id).toBe(55)
  })

  test('mapFields to change field to method on instance', () => {
    const schema = normalizeField({ test: () => true, value: Number })

    // convert all normal values to functions
    const object = mapFields({}, schema, (value, field) => {
      if (field.type == Number) {
        return () => false
      }
    })

    // map methods on objects types
    const object2 = mapFields(
      {},
      schema,
      (value, field) => {
        if (field.type === Object && value && field.methods) {
          Object.entries(field.methods).map(kvp => {
            value[kvp[0]] = () => false
          })
        }
      },
      true,
    )

    const object3 = mapFields({}, schema, (value, field) => {})

    expect(schema.methods.test()).toBe(true)

    expect(object.test).toBe(undefined)
    expect(object.value()).toBe(false)

    expect(object2.test()).toBe(false)
    expect(object2.value).toBe(undefined)

    expect(object3.test).toBe(undefined)
    expect(object3.value).toBe(undefined)
  })
})
describe('createObjects', () => {
  test('create nested objects', () => {
    const normNestedObject = normalizeField({
      meta: {
        submeta: {
          test: Boolean,
        },
      },
      metas: [
        {
          value: Number,
          test: e => e.value || true,
          submeta: {
            test: Boolean,
          },
        },
      ],
    })

    const object = createNestedObjects({}, normNestedObject)
    const object2 = createNestedObjects({}, normNestedObject, false)
    const object3 = createNestedObjects({ metas: [{}] }, normNestedObject)
    const object4 = createNestedObjects({ metas: [] }, normNestedObject)

    expect(object.metas).toBe(undefined)
    expect(typeof object.meta).toBe('object')
    expect(typeof object.meta.submeta).toBe('object')

    expect(typeof object2.meta).toBe('object')
    expect(object2.meta.submeta).toBe(undefined)

    expect(object3.metas).toHaveLength(1)
    expect(typeof object3.metas[0].submeta).toBe('object')

    expect(object4.metas).toHaveLength(0)

    expect(() => {
      createNestedObjects({}, normalizeField(String))
    }).toThrow()
  })
})
