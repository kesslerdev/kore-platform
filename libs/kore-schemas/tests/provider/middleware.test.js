import { Connect, callProviders, providers } from '../../src/providers'
import { normalizeField } from '../../src/schema'
import { Types } from '../../src/types'

const middleware = (value, field) => {
  if (!value && field.default !== undefined) {
    return field.default
  }
}

const middleware2 = (value, field) => {
  if (!value && field.type === Types.ObjectId) {
    return 55
  }
}
describe('bind manually providers', () => {
  test('only objects with methods', () => {
    const schema = normalizeField({
      id: {
        type: Types.ObjectId,
      },
      name: {
        type: String,
        default: 'John Doe',
      },
      jopa: Boolean,
    })

    const middlewares = new Connect()
    middlewares.add(middleware)
    middlewares.add(middleware2)

    const obj = {
      id: middlewares.handle(undefined, schema.fields.id),
      name: middlewares.handle(undefined, schema.fields.name),
      jopa: middlewares.handle(undefined, schema.fields.jopa),
    }

    expect(obj.id).toBe(55)
    expect(obj.name).toBe('John Doe')
    expect(obj.jopa).toBe(undefined)
  })
})

describe('bind providers', () => {
  test('default middleware', () => {
    const schema = normalizeField({
      id: {
        type: Types.ObjectId,
      },
      name: {
        type: String,
        default: 'John Doe',
      },
      meta: {
        type: Object,
        default: {},
        fields: {
          id: Types.ObjectId,
        },
      },
      metas: [
        {
          type: Object,
          default: {},
          fields: {
            id: Types.ObjectId,
          },
        },
      ],
    })

    const obj = { metas: [{}] }
    callProviders(obj, schema, providers)

    expect(typeof obj.id).toBe('string')
    expect(obj.name).toBe('John Doe')
    // default for object, then default for ObjectId
    expect(typeof obj.meta.id).toBe('string')
    expect(typeof obj.metas[0].id).toBe('string')
  })
})
