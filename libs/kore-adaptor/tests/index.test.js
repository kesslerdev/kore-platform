import { Adaptor } from '../src'

describe('Method #1', () => {
  test('Behavior #1', () => {
    const ad = new Adaptor({ __schema: {} })
    expect(ad.supportEvents()).toEqual([])
  })
})
