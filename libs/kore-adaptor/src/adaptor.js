// @flow
import { difference } from 'lodash'
import EventEmitter from 'events'
import type { EventAdaptor, SchemaObject } from './types'

/* eslint class-methods-use-this:
   ["error", { "exceptMethods": ["listenerUpdated","supportEvents"] }] */
/* eslint no-unused-vars: ["error", { "argsIgnorePattern": "^_" }] */
class Adaptor extends EventEmitter implements EventAdaptor<*> {
  schema: SchemaObject

  constructor(schema: SchemaObject) {
    super() // required
    this.schema = schema
    this.on('newListener', this.listenerUpdated)
    this.on('removeListener', this.listenerUpdated)
  }

  listenerUpdated(_event: string, _listener: Function) {}

  supportEvents() {
    return []
  }

  ensureSupportedEvents(eventNames: string[]) {
    const events = this.supportEvents()
    const missing = difference(eventNames, events)

    if (missing.length === 0) return true

    throw new Error(`Missing required event(s) ${missing.join(', ')}`)
  }
}

const EventTypes = {
  COLLECTION_UPDATED: 'collection_updated',
}
export { Adaptor, EventTypes }
