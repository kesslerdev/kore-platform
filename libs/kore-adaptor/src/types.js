// @flow

export interface Provider {}

export interface Schema {
  __providers?: [Provider];
}

export interface SchemaObject {
  __schema: Schema;
}

export interface ArrayAdaptor<T: SchemaObject> {
  all(): Promise<[T]>;
  add(value: T): Promise<boolean>;
}

export interface EventAdaptor<T: SchemaObject> {
  on(event: string, listener: Function): EventAdaptor<T>;
  once(event: string, listener: Function): EventAdaptor<T>;
  emit(event: string, ...args: Array<any>): boolean;
  listenerCount(event: string): number;
  supportEvents(): string[];
  ensureSupportedEvents(eventNames: string[]): boolean;
}

export interface FindAdaptor<T: SchemaObject> {
  findOne(criteria: Object): Promise<T>;
  findMany(criteria: Object): Promise<[T]>;
}
