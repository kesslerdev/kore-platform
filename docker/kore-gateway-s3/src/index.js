import express from 'express'
import { json } from 'body-parser'
import expressPlayground from 'graphql-playground-middleware-express'
import {
  restGatewayExpress,
  graphqlGatewayExpress,
  graphqlGatewayConfigurationExpress,
  gatewaySchema,
} from 'kore-gateway'
import AdaptorFs from 'kore-adaptor-fs';

(async () => {
  const app = express()
  const fsStorage = new AdaptorFs(gatewaySchema, { path: './conf' })

  app.use('/graphql', json(), graphqlGatewayExpress(fsStorage))
  app.use('/config', json(), graphqlGatewayConfigurationExpress(fsStorage))
  app.use('/api', json(), restGatewayExpress(fsStorage))

  app.use(
    '/playground',
    expressPlayground({
      folderName: 'Kore Platform',
      config: {
        extensions: {
          endpoints: {
            gateway: {
              url: 'http://localhost:3000/graphql',
            },
            config: {
              url: 'http://localhost:3000/config',
            },
          },
        },
      },
    }),
  )

  app.listen(3000)

  console.log(
    'Server running. Open http://localhost:3000/playground to run queries.',
  )
})(true)
