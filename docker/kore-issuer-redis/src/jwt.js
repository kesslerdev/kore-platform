import { sign } from 'kore-issuer-jwt'
import { ISSUER, ISSUER_PRIVATE_KEY } from './config'

const issueJWT = options => (req, res) => {
  const token = sign({}, ISSUER_PRIVATE_KEY, {
    subject: req.user.id,
    issuer: ISSUER,
    ...options,
  })

  res.json({
    user: req.user.toPublicObject(),
    token,
  })
}

export default issueJWT
