import { Strategy } from 'passport-local'

import {
  Schemas,
  createLocalStrategyCallback,
  createMemoryStorageAdapter,
} from 'kore-issuer-passport'

// import schema from schemas libs + merge schema on issuer schema (for local use only)
const memory = createMemoryStorageAdapter(Schemas.User) // need to be shared
const callback = createLocalStrategyCallback(memory)

const localStrategy = new Strategy(callback)

export default localStrategy
