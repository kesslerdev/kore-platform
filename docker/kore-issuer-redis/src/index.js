import express from 'express'
import passport from 'passport'
import { json } from 'body-parser'

import local from './strategies/local'
import issueJWT from './jwt';

(async () => {
  const app = express()

  app.use(json())
  app.use(passport.initialize())

  passport.use(local)

  app.post(
    '/issue',
    passport.authenticate('local', { session: false }),
    issueJWT(),
  )

  app.listen(8000)

  console.log(
    'Server running. Open http://localhost:8000/issue to issue a token.',
  )
})(true)
