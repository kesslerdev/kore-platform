import fs from 'fs'

const ISSUER = 'kore:issuer'
const ISSUER_PRIVATE_KEY = fs.readFileSync('jwtRS256.key')
const ISSUER_PUBLIC_KEY = fs.readFileSync('jwtRS256.key.pub')
export { ISSUER, ISSUER_PRIVATE_KEY, ISSUER_PUBLIC_KEY }
