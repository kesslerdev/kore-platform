import uuidv4 from 'uuid/v4'
import hasher from '../hasher'

const userTransformer = (user) => {
  if(!user.id) {
    user = Object.assign({}, user, {
      id: uuidv4()
    })
  }

  if(user.accesses) {
    user.accesses = user.accesses.map((access) => {
      if (access.kind == 'LOCAL') {
        return Object.assign({},
          {kind: access.kind},
          {local: hasher.hash(access.password, user.id)}
        )
      }

      return access
    })
  }

  return user
}

export default userTransformer
