import {inspect} from 'util'
import {user} from 'kore-schemas'
import kafka from 'kafka-node'

import createProducer from './producer'
import Importer from './app'
import userTransformer from './transformer/users'

const imp = new Importer()
imp.setSchema('users', user)
imp.addTransformer('users', userTransformer)

const payloads =  imp.getPayloads()

console.log(inspect(payloads, false, null))

const client = new kafka.KafkaClient({kafkaHost: 'localhost:9094'})

createProducer(client).then((producer) => {
  console.log('connected')
  producer.producer.send(payloads, (err, result) => {
    console.log('sended')
    console.log(err || result)
    process.exit()
  })
})
