import createHash from 'kore-hash'

const hasher = createHash('MYSECRET')

export default hasher
