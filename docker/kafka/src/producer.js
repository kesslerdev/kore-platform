import {Producer} from 'kafka-node'

const produceService = (producer) => {
  return {
    producer,
    send: (topic, messages) => {
      return new Promise((resolve, reject) => {
        producer.send([{ topic, messages }], (err, data) => {
            if(err) {
              reject(err)
            } else {
              resolve(data)
            }
        })
      })
    }
  }
}

const createProducer = (kafkaClient) => {

  return new Promise((resolve, reject) => {
    const producer = new Producer(kafkaClient)

    producer.on('ready', () => {
      resolve(produceService(producer))
    })
    producer.on('error', (err) => {
      reject(err)
    })
  })
}

export default createProducer
