import {sync as globSync} from 'glob'
import path from 'path'
import fs from 'fs'
import {KeyedMessage} from 'kafka-node'

const PATTERN = 'data/*/*.json'

const transform = (base, transformers) => {
  return transformers.reduce((value, transformer)=>{
    return transformer(value)
  }, base)
}

const find = (files) => {
  // reset found files
  const topics = []
  for(var i= 0; i < files.length; i++) {
    const parts = files[i].split(path.sep)
    const topic = parts[parts.length-2]

    if(!topics[topic]) {
      topics[topic] = []
    }

    topics[topic].push(files[i])
  }

  return topics
}

class Importer{
  constructor(pattern = PATTERN) {
    this._pattern = pattern
    this._trans = []
    this._schemas = []
    this._topics = find(globSync(PATTERN))
  }

  addTransformer(topic, transformer) {
    if(!this._trans[topic]) {
      this._trans[topic] = []
    }

    this._trans[topic].push(transformer)
  }

  setSchema(topic, schema) {
    this._schemas[topic] = schema
  }

  buildMessage(topic, file) {
    console.log(`Build message from file ${file} for topic ${topic}`)
    //getcontent
    const obj = JSON.parse(fs.readFileSync(file,'utf8'))
    //transformer
    const final = transform(obj, this._trans[topic] || [])
    return final
  }

  getPayloads() {
    const payloads = []
    Object.keys(this._topics).map(async (topic) => {
      const files = this._topics[topic]
      const messages = []

      for(var i= 0; i < files.length; i++) {
        const rawMessage = this.buildMessage(topic, files[i])
        const binMessage = this._schemas[topic].toBuffer(rawMessage)

        if(rawMessage.id) {
          messages.push(new KeyedMessage(rawMessage.id, binMessage))
        } else {
          messages.push(binMessage)
        }
      }
      payloads.push({topic, messages})
    })

    return payloads
  }
}

export default Importer
