/* eslint new-cap: 0 */
/* eslint class-methods-use-this:
   ["error", { "exceptMethods": ["listenerUpdated","supportEvents"] }] */

import { Adaptor, EventTypes } from 'kore-adaptor'
import type { ArrayAdaptor, SchemaObject } from 'kore-adaptor'

import uuidv4 from 'uuid/v4'
import {
  accessSync,
  readdirSync,
  readFileSync,
  writeFileSync,
  constants,
} from 'fs'
import { watch } from 'chokidar'
import { join } from 'path'

class AdaptorFs extends Adaptor implements ArrayAdaptor {
  path: string

  fsMode: string

  watcher

  constructor(schema: SchemaObject, { path, fsMode }) {
    super(schema) // required
    this.path = path
    this.fsMode = fsMode || constants.R_OK | constants.W_OK
    accessSync(path, fsMode)
    this.watcher = null
  }

  canRead() {
    return (this.fsMode & constants.R_OK) === constants.R_OK
  }

  canWrite() {
    return (this.fsMode & constants.W_OK) === constants.W_OK
  }

  ensureRead() {
    if (!this.canRead()) throw new Error('Read is forbidden')
  }

  ensureWrite() {
    if (!this.canWrite()) throw new Error('Write is forbidden')
  }

  createObject(path = null) {
    if (!path) return new this.schema()

    return new this.schema(JSON.parse(readFileSync(path, 'utf8')))
  }

  supportEvents() {
    return [EventTypes.COLLECTION_UPDATED]
  }

  listenerUpdated(event, _listener) {
    switch (event) {
      case EventTypes.COLLECTION_UPDATED:
        if (this.listenerCount(event) > 0 && this.watcher === null) {
          // enable watch
          this.watcher = watch(this.path)
          this.watcher.on('all', async (_eventType, _filename) => {
            this.emit(EventTypes.COLLECTION_UPDATED, await this.all())
          })
        } else if (this.watcher) {
          // disable watch
          this.watcher.close()
          this.watcher = null
        }
        break
      default:
        console.error(`Unsupported event ${event}`)
        break
    }
  }

  async all() {
    this.ensureRead()

    return readdirSync(this.path).map(file => this.createObject(join(this.path, file)))
  }

  async add(schemaObject) {
    this.ensureWrite()

    const obj = schemaObject.toObject
      ? schemaObject.toObject()
      : new this.schema(schemaObject).toObject()
    const id = obj.id || uuidv4()
    const filename = join(this.path, `${id}.json`)

    writeFileSync(filename, JSON.stringify(obj))

    return true
  }
}

export default AdaptorFs
