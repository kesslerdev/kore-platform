import fs from 'fs'
import { sign, verify } from '../src'

const ISSUER_PRIVATE_KEY = fs.readFileSync('tests/jwtRS256.key')
const ISSUER_PUBLIC_KEY = fs.readFileSync('tests/jwtRS256.key.pub')
const jwtid = 'c3258d5c-3dec-44ad-81ff-d62dd273ed8b'
const issuer = 'test'

describe('Method Sign', () => {
  const token = sign({ roles: ['ADMIN'] }, ISSUER_PRIVATE_KEY, { jwtid })

  test('Behavior issue valid token', () => {
    expect(typeof token).toBe(typeof '')

    expect(() => {
      verify(token, ISSUER_PUBLIC_KEY)
    }).not.toThrow()
  })
})

describe('Method Verify', () => {
  const token = sign({ roles: ['ADMIN'] }, ISSUER_PRIVATE_KEY, {
    jwtid,
    issuer,
  })

  test('Behavior verify valid token issuer', () => {
    expect(() => {
      verify(token, ISSUER_PUBLIC_KEY, { issuer })
    }).not.toThrow()
  })

  test('Behavior throw if invalid token', () => {
    expect(() => {
      verify('', ISSUER_PUBLIC_KEY)
    }).toThrow()
  })

  test('Behavior extract token', () => {
    const iss = verify(token, ISSUER_PUBLIC_KEY)

    expect(iss.jti).toBe(jwtid)
    expect(iss.roles[0]).toBe('ADMIN')
  })
})
