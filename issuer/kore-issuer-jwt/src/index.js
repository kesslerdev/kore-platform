import jwt from 'jsonwebtoken'
import uuidv4 from 'uuid/v4'

const verify = (token, publicCertificate, options) => jwt.verify(token, publicCertificate, {
  algorithms: ['RS256'],
  ...options,
})

const sign = (payload, privateCertificate, options) => jwt.sign(payload, privateCertificate, {
  algorithm: 'RS256',
  expiresIn: '1d',
  jwtid: uuidv4(),
  ...options,
})

export { verify, sign }
