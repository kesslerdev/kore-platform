// @flow

export interface IUser {
  username: string;

  validPassword(password: string): boolean;
}

export interface IIssuerStorageAdapter {
  findOne(user: {
    username?: string,
    id?: string,
  }): Promise<IUser>;

  findOrCreate(user: {
    provider: string,
    profileId: string,
  }): Promise<IUser>;
}
