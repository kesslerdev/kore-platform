/* eslint new-cap: 0 */
import { find } from 'lodash'
import type { IIssuerStorageAdapter, IUser } from './types'
import hasher from '../schemas/hasher'

// TODO: NEED SCHEMA (for user) infer from interface

// TODO: use function to ensure userSchema is a subtype of userSchema (from local types)
const createMemoryStorageAdapter = (userSchema): IIssuerStorageAdapter => {
  // check if schema respect the schema of ../schema/user (as little)
  const kessler = new userSchema({
    username: 'kessler',
    accesses: [{ kind: 'LOCAL', local: hasher.hash('123456', 'kessler') }],
  })
  const users = [kessler]

  return {
    findOne: async filter => {
      console.log('find user', filter)
      return find(users, filter)
    },
    findOrCreate: async ({ provider, profileId }) => {
      let user = users.find((user: IUser) => user.accesses.find(
        access => access.kind === 'OAUTH'
            && access.oauth.profileId === profileId
            && access.oauth.provider === provider,
      ))

      if (!user) {
        user = new userSchema({
          accesses: [{ kind: 'OAUTH', oauth: { provider, profileId } }],
        })
        users.push(user)
      }

      return user
    },
  }
}

export default createMemoryStorageAdapter
