import createLocalStrategyCallback from './strategies/local'
import createMemoryStorageAdapter from './adapters/memory'
import User from './schemas/user'

const Schemas = {
  User,
}
export { createLocalStrategyCallback, createMemoryStorageAdapter, Schemas }
