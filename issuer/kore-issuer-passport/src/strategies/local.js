// @flow
import type { IIssuerStorageAdapter } from '../adapters/types'

const createLocalStrategyCallback = (
  storageAdapter: IIssuerStorageAdapter,
) => async (username: string, password: string, done: Function) => {
  const user = await storageAdapter.findOne({ username })

  if (!user) {
    return done(null, false, { message: 'Incorrect username.' })
  }

  if (!user.validPassword(password)) {
    return done(null, false, { message: 'Incorrect password.' })
  }

  return done(null, user)
}

export default createLocalStrategyCallback
