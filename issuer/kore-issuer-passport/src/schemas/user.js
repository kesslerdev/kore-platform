import { Types, buildSchema } from 'kore-schemas'
import hasher from './hasher'

const schema = {
  __type: {
    name: 'User',
    description: 'User schema',
  },
  id: Types.ObjectId,
  username: String,
  roles: [String],
  validPassword: (user, password) => {
    if (user.accesses) {
      const access = user.accesses.find(element => element.kind === 'LOCAL')

      return hasher.verify(password, access.local, user.username)
    }

    return false
  },
  accesses: {
    type: Array,
    items: {
      __type: {
        name: 'StrategyAccess',
        description:
          'Represent an access for this user it can be local (with password), using oauth or custom provider',
      },
      kind: {
        type: String,
        // without usage of providers magic no custom validation on object
        // but it can be used by Translators if support usage
        enum: ['LOCAL', 'OAUTH', 'CUSTOM'],
      },
      local: {
        default: null,
        type: Object,
        private: true,
        fields: {
          hash: String,
          salt: String,
        },
      },
      getData: access => {
        switch (access.kind) {
          case 'LOCAL':
            return access.local
          case 'OAUTH':
            return access.oauth
          case 'CUSTOM':
            return access.custom
          default:
            return false
        }
      },
      oauth: {
        __type: {
          default: null,
        },
        profileId: String,
        provider: String,
        accessToken: { type: String, private: true },
        refreshToken: { type: String, private: true },
      },
      custom: {
        type: Types.Mixed, // discouraged
        default: null,
      },
    },
  },
  organizations: [
    {
      organization: Types.ObjectId,
      grade: String,
    },
  ],
  meta: {
    __type: {
      default: null,
    },
    organization: Types.ObjectId,
    grade: String,
  },
}

const User = buildSchema(schema)

export default User
