// @flow

import {
  makeRemoteExecutableSchema,
  mergeSchemas,
  introspectSchema,
} from 'graphql-tools'
import { HttpLink } from 'apollo-link-http'
import fetch from 'node-fetch'
import type { IService } from '../adapters/types'
import buildMetadataSchema from './metadata'

const createRemoteSchema = async uri => {
  const link = new HttpLink({ uri, fetch })
  const schema = await introspectSchema(link)

  return makeRemoteExecutableSchema({
    schema,
    link,
  })
}

const buildGatewaySchema = async (
  services: IService[],
  withMetadata: ?boolean = true,
) => {
  const schemas = []
  await Promise.all(
    services.map(async service => {
      if (service.schemas) {
        schemas.push(
          ...(await Promise.all(
            service.schemas.map(schema => createRemoteSchema(schema.endpoint)),
          )),
        )
      }
    }),
  )

  if (withMetadata) {
    schemas.push(buildMetadataSchema(services))
  }

  return mergeSchemas({
    schemas,
  })
}

export default buildGatewaySchema
