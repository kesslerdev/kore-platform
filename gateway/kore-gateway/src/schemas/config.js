// @flow
import { makeExecutableSchema } from 'graphql-tools'
import type { GatewayAdaptor } from '../adapters/types'

const typeDefs = `
  type Service {
    name: String
    schemas: [Schema]
  }

  type Schema {
    endpoint: String
  }

  type Query {
    services: [Service]
  }

  type Mutation {
    createService(input: CreateServiceInput!): Service
  }

  input CreateServiceInput {
    name: String!
    schemas: [SchemaInput]!
  }

  input SchemaInput{
    endpoint: String!
  }
`
// TODO: logging system .. winston ?
// console.log(`add new service ${input.name}`)
const buildConfigurationSchema = (adaptor: GatewayAdaptor) => {
  const resolvers = {
    Query: { services: () => adaptor.all() },
    Mutation: {
      createService: async (root, { input }) => (await adaptor.add(input)) ? input : null,
    },
  }

  return makeExecutableSchema({
    typeDefs,
    resolvers,
  })
}

export default buildConfigurationSchema
