// @flow
import { makeExecutableSchema } from 'graphql-tools'
import type { IService } from '../adapters/types'

const typeDefs = `
  type Query { _services: [String] }
`

const buildMetadataSchema = (services: IService[]) => {
  const resolvers = {
    Query: { _services: () => services.map(e => e.name) },
  }

  return makeExecutableSchema({
    typeDefs,
    resolvers,
  })
}

export default buildMetadataSchema
