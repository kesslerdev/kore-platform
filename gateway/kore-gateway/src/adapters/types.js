// @flow
import type { EventAdaptor, ArrayAdaptor } from 'kore-adaptor'
import { buildSchema } from 'kore-schemas'

export interface ISchema {
  endpoint: string;
}

// TODO: need to be inferred from schema
export interface IService {
  name: string;
  schemas: [ISchema];
  httpProxy: string;
}

export type GatewayAdaptor = EventAdaptor & ArrayAdaptor

const gatewaySchemaDefinition = {
  name: String,
  schemas: [
    {
      endpoint: String,
    },
  ],
  httpProxy: String,
}

const gatewaySchema = buildSchema(gatewaySchemaDefinition)

export { gatewaySchemaDefinition, gatewaySchema }
