// @flow
import graphqlGatewayExpress from './middlewares/gateway'
import restGatewayExpress from './middlewares/rest'
import graphqlGatewayConfigurationExpress from './middlewares/config'
import buildGatewaySchema from './schemas/gateway'
import buildConfigurationSchema from './schemas/config'
import { gatewaySchema } from './adapters/types'

module.exports = {
  graphqlGatewayExpress,
  graphqlGatewayConfigurationExpress,
  restGatewayExpress,
  buildGatewaySchema,
  buildConfigurationSchema,
  gatewaySchema,
}
