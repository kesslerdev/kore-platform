// @flow
import type { Middleware } from 'express'
import { graphqlExpress } from 'apollo-server-express'
import type { GatewayAdaptor } from '../adapters/types'

import buildConfigurationSchema from '../schemas/config'

const graphqlGatewayConfigurationExpress = (
  adaptor: GatewayAdaptor,
): Middleware => graphqlExpress({
  schema: buildConfigurationSchema(adaptor),
  tracing: true,
})

export default graphqlGatewayConfigurationExpress
