// @flow
import type {
  Middleware, $Request, $Response, NextFunction,
} from 'express'
import { EventTypes } from 'kore-adaptor'

import { graphqlExpress } from 'apollo-server-express'
import buildGatewaySchema from '../schemas/gateway'
import type { GatewayAdaptor } from '../adapters/types'

const graphqlGatewayExpress = (
  adaptor: GatewayAdaptor,
  withMetadata: ?boolean = true,
): Middleware => {
  adaptor.ensureSupportedEvents([EventTypes.COLLECTION_UPDATED])
  let graphqlMiddleware

  adaptor.on(EventTypes.COLLECTION_UPDATED, async services => {
    // TODO: logging system .. winston ? console.log('update graphql schema')
    graphqlMiddleware = graphqlExpress({
      schema: await buildGatewaySchema(services, withMetadata),
      tracing: true,
    })
  })

  adaptor.all().then(services => {
    adaptor.emit(EventTypes.COLLECTION_UPDATED, services)
  })

  return (req: $Request, res: $Response, next: NextFunction) => {
    graphqlMiddleware(req, res, next)
  }
}

export default graphqlGatewayExpress
