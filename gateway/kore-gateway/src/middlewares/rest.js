// @flow
import type {
  Middleware, $Request, $Response, NextFunction,
} from 'express'
import { EventTypes } from 'kore-adaptor'

import express from 'express'
import proxy from 'express-http-proxy'
import type { GatewayAdaptor, IService } from '../adapters/types'

const buildRestRouter = async (services: IService[]) => {
  const router = express.Router()

  await Promise.all(
    services.map(async service => {
      if (service.httpProxy) {
        router.use(`/${service.name}`, proxy(service.httpProxy))
      }
    }),
  )

  return router
}

const restGatewayExpress = (adaptor: GatewayAdaptor): Middleware => {
  adaptor.ensureSupportedEvents([EventTypes.COLLECTION_UPDATED])
  let router
  adaptor.on(EventTypes.COLLECTION_UPDATED, async services => {
    // TODO: logging system .. winston ? console.log('update rest schema')

    router = await buildRestRouter(services)
  })

  return (req: $Request, res: $Response, next: NextFunction) => {
    router(req, res, next)
  }
}

export default restGatewayExpress
