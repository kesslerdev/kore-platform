// flow-typed signature: d8e9ac9d657b9e673e7e6f6f04109fb0
// flow-typed version: <<STUB>>/kore-adaptor_v^0.1.0/flow_v0.75.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'kore-adaptor'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'kore-adaptor' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module 'kore-adaptor/coverage/lcov-report/block-navigation' {
  declare module.exports: any;
}

declare module 'kore-adaptor/coverage/lcov-report/prettify' {
  declare module.exports: any;
}

declare module 'kore-adaptor/coverage/lcov-report/sorter' {
  declare module.exports: any;
}

declare module 'kore-adaptor/dist/adaptor' {
  declare module.exports: any;
}

declare module 'kore-adaptor/dist/index' {
  declare module.exports: any;
}

declare module 'kore-adaptor/dist/types' {
  declare module.exports: any;
}

declare module 'kore-adaptor/flow-typed/npm/@babel/cli_vx.x.x' {
  declare module.exports: any;
}

declare module 'kore-adaptor/flow-typed/npm/@babel/core_vx.x.x' {
  declare module.exports: any;
}

declare module 'kore-adaptor/flow-typed/npm/@babel/plugin-proposal-object-rest-spread_vx.x.x' {
  declare module.exports: any;
}

declare module 'kore-adaptor/flow-typed/npm/@babel/plugin-proposal-optional-chaining_vx.x.x' {
  declare module.exports: any;
}

declare module 'kore-adaptor/flow-typed/npm/@babel/preset-env_vx.x.x' {
  declare module.exports: any;
}

declare module 'kore-adaptor/flow-typed/npm/@babel/preset-flow_vx.x.x' {
  declare module.exports: any;
}

declare module 'kore-adaptor/flow-typed/npm/babel-core_vx.x.x' {
  declare module.exports: any;
}

declare module 'kore-adaptor/flow-typed/npm/babel-eslint_vx.x.x' {
  declare module.exports: any;
}

declare module 'kore-adaptor/flow-typed/npm/babel-jest_vx.x.x' {
  declare module.exports: any;
}

declare module 'kore-adaptor/flow-typed/npm/eslint_vx.x.x' {
  declare module.exports: any;
}

declare module 'kore-adaptor/flow-typed/npm/eslint-config-airbnb_vx.x.x' {
  declare module.exports: any;
}

declare module 'kore-adaptor/flow-typed/npm/eslint-plugin-import_vx.x.x' {
  declare module.exports: any;
}

declare module 'kore-adaptor/flow-typed/npm/eslint-plugin-jest_vx.x.x' {
  declare module.exports: any;
}

declare module 'kore-adaptor/flow-typed/npm/eslint-plugin-jsx-a11y_vx.x.x' {
  declare module.exports: any;
}

declare module 'kore-adaptor/flow-typed/npm/eslint-plugin-react_vx.x.x' {
  declare module.exports: any;
}

declare module 'kore-adaptor/flow-typed/npm/flow-bin_v0.x.x' {
  declare module.exports: any;
}

declare module 'kore-adaptor/flow-typed/npm/jest_v23.x.x' {
  declare module.exports: any;
}

declare module 'kore-adaptor/flow-typed/npm/lodash_v4.x.x' {
  declare module.exports: any;
}

declare module 'kore-adaptor/src/adaptor' {
  declare module.exports: any;
}

declare module 'kore-adaptor/src/index' {
  declare module.exports: any;
}

declare module 'kore-adaptor/src/types' {
  declare module.exports: any;
}

declare module 'kore-adaptor/tests/index.test' {
  declare module.exports: any;
}

// Filename aliases
declare module 'kore-adaptor/coverage/lcov-report/block-navigation.js' {
  declare module.exports: $Exports<'kore-adaptor/coverage/lcov-report/block-navigation'>;
}
declare module 'kore-adaptor/coverage/lcov-report/prettify.js' {
  declare module.exports: $Exports<'kore-adaptor/coverage/lcov-report/prettify'>;
}
declare module 'kore-adaptor/coverage/lcov-report/sorter.js' {
  declare module.exports: $Exports<'kore-adaptor/coverage/lcov-report/sorter'>;
}
declare module 'kore-adaptor/dist/adaptor.js' {
  declare module.exports: $Exports<'kore-adaptor/dist/adaptor'>;
}
declare module 'kore-adaptor/dist/index.js' {
  declare module.exports: $Exports<'kore-adaptor/dist/index'>;
}
declare module 'kore-adaptor/dist/types.js' {
  declare module.exports: $Exports<'kore-adaptor/dist/types'>;
}
declare module 'kore-adaptor/flow-typed/npm/@babel/cli_vx.x.x.js' {
  declare module.exports: $Exports<'kore-adaptor/flow-typed/npm/@babel/cli_vx.x.x'>;
}
declare module 'kore-adaptor/flow-typed/npm/@babel/core_vx.x.x.js' {
  declare module.exports: $Exports<'kore-adaptor/flow-typed/npm/@babel/core_vx.x.x'>;
}
declare module 'kore-adaptor/flow-typed/npm/@babel/plugin-proposal-object-rest-spread_vx.x.x.js' {
  declare module.exports: $Exports<'kore-adaptor/flow-typed/npm/@babel/plugin-proposal-object-rest-spread_vx.x.x'>;
}
declare module 'kore-adaptor/flow-typed/npm/@babel/plugin-proposal-optional-chaining_vx.x.x.js' {
  declare module.exports: $Exports<'kore-adaptor/flow-typed/npm/@babel/plugin-proposal-optional-chaining_vx.x.x'>;
}
declare module 'kore-adaptor/flow-typed/npm/@babel/preset-env_vx.x.x.js' {
  declare module.exports: $Exports<'kore-adaptor/flow-typed/npm/@babel/preset-env_vx.x.x'>;
}
declare module 'kore-adaptor/flow-typed/npm/@babel/preset-flow_vx.x.x.js' {
  declare module.exports: $Exports<'kore-adaptor/flow-typed/npm/@babel/preset-flow_vx.x.x'>;
}
declare module 'kore-adaptor/flow-typed/npm/babel-core_vx.x.x.js' {
  declare module.exports: $Exports<'kore-adaptor/flow-typed/npm/babel-core_vx.x.x'>;
}
declare module 'kore-adaptor/flow-typed/npm/babel-eslint_vx.x.x.js' {
  declare module.exports: $Exports<'kore-adaptor/flow-typed/npm/babel-eslint_vx.x.x'>;
}
declare module 'kore-adaptor/flow-typed/npm/babel-jest_vx.x.x.js' {
  declare module.exports: $Exports<'kore-adaptor/flow-typed/npm/babel-jest_vx.x.x'>;
}
declare module 'kore-adaptor/flow-typed/npm/eslint_vx.x.x.js' {
  declare module.exports: $Exports<'kore-adaptor/flow-typed/npm/eslint_vx.x.x'>;
}
declare module 'kore-adaptor/flow-typed/npm/eslint-config-airbnb_vx.x.x.js' {
  declare module.exports: $Exports<'kore-adaptor/flow-typed/npm/eslint-config-airbnb_vx.x.x'>;
}
declare module 'kore-adaptor/flow-typed/npm/eslint-plugin-import_vx.x.x.js' {
  declare module.exports: $Exports<'kore-adaptor/flow-typed/npm/eslint-plugin-import_vx.x.x'>;
}
declare module 'kore-adaptor/flow-typed/npm/eslint-plugin-jest_vx.x.x.js' {
  declare module.exports: $Exports<'kore-adaptor/flow-typed/npm/eslint-plugin-jest_vx.x.x'>;
}
declare module 'kore-adaptor/flow-typed/npm/eslint-plugin-jsx-a11y_vx.x.x.js' {
  declare module.exports: $Exports<'kore-adaptor/flow-typed/npm/eslint-plugin-jsx-a11y_vx.x.x'>;
}
declare module 'kore-adaptor/flow-typed/npm/eslint-plugin-react_vx.x.x.js' {
  declare module.exports: $Exports<'kore-adaptor/flow-typed/npm/eslint-plugin-react_vx.x.x'>;
}
declare module 'kore-adaptor/flow-typed/npm/flow-bin_v0.x.x.js' {
  declare module.exports: $Exports<'kore-adaptor/flow-typed/npm/flow-bin_v0.x.x'>;
}
declare module 'kore-adaptor/flow-typed/npm/jest_v23.x.x.js' {
  declare module.exports: $Exports<'kore-adaptor/flow-typed/npm/jest_v23.x.x'>;
}
declare module 'kore-adaptor/flow-typed/npm/lodash_v4.x.x.js' {
  declare module.exports: $Exports<'kore-adaptor/flow-typed/npm/lodash_v4.x.x'>;
}
declare module 'kore-adaptor/src/adaptor.js' {
  declare module.exports: $Exports<'kore-adaptor/src/adaptor'>;
}
declare module 'kore-adaptor/src/index.js' {
  declare module.exports: $Exports<'kore-adaptor/src/index'>;
}
declare module 'kore-adaptor/src/types.js' {
  declare module.exports: $Exports<'kore-adaptor/src/types'>;
}
declare module 'kore-adaptor/tests/index.test.js' {
  declare module.exports: $Exports<'kore-adaptor/tests/index.test'>;
}
