# Kore Platform (KP)

## Kore Services

### Kore Gateway (GraphQL/HTTP)

exposed to external trafic, forward request to all other services not exposed.

Forwared protocols:
- HTTP, simple http requests or rest
- GraphQL, merge all schemas from other service in one base schema (allow requesting data from multiple services in one graphql query)

Projects:

- `kore-gateway`: `docker/gateway` - final usable App
- `kore-lib-gateway`: `gateway/gateway` - express middlewares for graphql/http
- `kore-lib-gateway-{adapter}` : `gateway/gateway-{adapter}` - storage adapter for {adapter} (ex: redis, mongoose, ...)

### Kore Discovery

using different platform adapters, find and register services in gateway

Projects:

- `kore-discovery`: `docker/discovery` - final usable App
- `kore-lib-discovery`: `discovery/discovery` - base services to communicate with gateway
- `kore-lib-discovery-{adapter}` : `discovery/discovery-{adapter}` - discovery adpater for {adapter} (ex: rancher, kubernetes, zookeeper, swarm, ...)

### Kore Issuer

using different strategies, log on user, with password, oauth,... with mfa support and user creation

Projects:

- `kore-issuer`: `docker/issuer` - final usable App
- `kore-lib-issuer`: `issuer/issuer` - base services to logon users
- `kore-lib-issuer-{adapter}` : `issuer/issuer-{adapter}` - storage adapter for {adapter} (ex: redis, mongoose, ...)


## Kore Libs

### Kore schemas



## ops

https://immoweltgroup.gitbooks.io/flow-mono-cli/docs/cli/install-types.html
